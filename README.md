# X Nuances de gris

Quentin BERNARD

## Description du projet

"X Nuance de Gris" est un projet informatique qui a été développé dans le but de créer une application pratique appelée Color Picker. L'application a été conçue en Java, en utilisant notamment JavaFX pour la conception de l'interface utilisateur.

Le contexte à l'origine de ce projet est le problème que l'on rencontre parfois lors de l'impression de documents en couleur, mais en version noir et blanc. Dans de telles situations, il peut être difficile de distinguer les différentes nuances de gris présentes dans le document. C'est là que notre application "X Nuance de Gris" intervient.

L'application "X Nuance de Gris" offre une solution pratique en permettant aux utilisateurs de sélectionner et d'explorer une large gamme de nuances de gris. En utilisant le Color Picker, les utilisateurs peuvent identifier précisément la valeur de gris souhaitée pour leur document, facilitant ainsi la visualisation et l'utilisation de différentes nuances de gris.

Ce projet a permis aux développeurs de mettre en pratique leurs compétences en programmation Java et en conception d'interfaces utilisateur avec JavaFX. Ils ont travaillé en équipe pour créer une application fonctionnelle, intuitive et esthétiquement plaisante, offrant une solution pratique aux problèmes liés à la visualisation des nuances de gris.

Vidéo de présentation : lien

## Fonctionnalités

1 - Sélection dynamique des couleurs :
L'application permet à l'utilisateur de choisir jusqu'à trois couleurs initiales.
Il est possible d'ajouter dynamiquement des couleurs supplémentaires jusqu'à un maximum de dix.
Chaque couleur sélectionnée sera convertie en une nuance de gris correspondante.

2 - Conversion en nuances de gris :
Les couleurs sélectionnées sont converties en nuances de gris de manière précise.
L'application génère automatiquement les valeurs de gris correspondantes pour chaque couleur choisie.

3 - Graphique circulaire de comparaison :
L'application présente un graphique circulaire comparatif qui affiche côte à côte les couleurs sélectionnées précédemment.
Ce graphique permet à l'utilisateur de visualiser les nuances de gris correspondantes pour chaque couleur et de les comparer directement.

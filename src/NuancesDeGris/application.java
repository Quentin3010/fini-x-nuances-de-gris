package NuancesDeGris;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * Classe principale du projet
 * @author Quentin BERNARD et Hugo ROUX
 * @version 04/06/2021
 */

public class application extends Application{
	//Variable qui définit le nombre de carré disponibles
	public static int nbNuance = 0;
	//initialisaton de la list stockant les couleurs
	List<String> couleurs = new ArrayList<String>();
	//Initialisation d'une nouvelle Hbox
	static HBox GraphsHorizontale = new HBox();

	@Override
	public void start(Stage stage) {
		//On remplace le logo de base par un nouveau
		stage.getIcons().setAll(new Image(getClass().getResource("WindowLogo.png").toExternalForm()));
		
		//Création et initialisation d'une Vbox
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.setPrefWidth(500);
		vbox.setPrefHeight(491);
    	
		//on ajoute 3 couleurs par défaut
		couleurs.add("ff0000");
		couleurs.add("00ff00");
		couleurs.add("0000ff");
		
		//On crée les camemberts
		PieChart chart = updateGraphique(false);
		PieChart chart2 = updateGraphique(true);
		
		//on retire les légendes des camemeberts (non nécessaires ici)
	    chart.setLegendVisible(false);
	    chart2.setLegendVisible(false);
		
	    //Initialisation d'un rectangle pour chaque couleur (ici 3)
		Rectangle r1 = createRectangle(couleurs.get(0));
		Rectangle r2 = createRectangle(couleurs.get(1));
		Rectangle r3 = createRectangle(couleurs.get(2));
		
		//Ajout des rectangles et de leur conversion en gris
    	vbox.getChildren().add(createHBox(r1, couleurs.get(0), vbox, stage));
    	vbox.getChildren().add(createHBox(r2, couleurs.get(1), vbox, stage));
    	vbox.getChildren().add(createHBox(r3, couleurs.get(2), vbox, stage));
    	
    	//Ajout d'un scroller
    	ScrollPane Couleur = new ScrollPane();
    	Couleur.setContent(vbox);
    	Couleur.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
    	
    	//Création du bouton "AjouterCouleur"
    	Button ajouterCouleur = new Button("+");
    	ajouterCouleur.setStyle("-fx-font: 30px Verdana;fx-font: 30px Verdana;");
    	VBox.setMargin(ajouterCouleur, new Insets(20));
    	
    	//Création du bouton "retirerCouleur"
    	Button retirerCouleur = new Button("-");
    	retirerCouleur.setStyle("-fx-font: 30px Verdana;fx-font: 30px Verdana;");
    	VBox.setMargin(retirerCouleur, new Insets(20));
    	
    	//Ajout de l'évènement "setOnMouseReleased" 
    	//sur le bouton "ajouterCouleur"
    	ajouterCouleur.setOnMouseReleased(e->{
    		//Ajout d'une nouvelle couleur dans la liste
    		couleurs.add("ffffff");
    		Rectangle r4 = createRectangle("ffffff");
    		
    		//Ajout des rectangles et de leur conversion en gris
    		vbox.getChildren().add(vbox.getChildren().size()-1, createHBox(r4, "ffffff", vbox, stage));
    		
    		//On change le nom de la fenêtre en fonction du nombre de lignes de conversions de couleur
    		if(nbNuance<=1) stage.setTitle(nbNuance+" Nuance de Gris");
    		else stage.setTitle(nbNuance+" Nuances de Gris");
    		
    		//Si on a 10 lignes de conversions de couleur
    		if(nbNuance==10) {
    			//On retire l'option d'en ajouter une nouvelle
    			vbox.getChildren().get(vbox.getChildren().size()-1).setVisible(false);
    		}
    		
    		//On vide GraphsHorizontale, qui stockait les camemberts
    		GraphsHorizontale.getChildren().clear();
    		//On ajoute des camemberts avec de nouvelles données
    		GraphsHorizontale.getChildren().add(updateGraphique(false));
        	GraphsHorizontale.getChildren().add(updateGraphique(true));
    		
    	});
    	
    	//Ajout du bouton "AjouterCouleur"
    	HBox boutonPlus = new HBox();
    	boutonPlus.getChildren().addAll(new Label("             "), ajouterCouleur);
    	boutonPlus.setAlignment(Pos.CENTER);
    	VBox.setMargin(boutonPlus, new Insets(20));
    	vbox.getChildren().addAll(boutonPlus);
    		    
    	//Affichage des deux graphiques
    	GraphsHorizontale.setAlignment(Pos.CENTER_LEFT);
    	VBox.setMargin(GraphsHorizontale, new Insets(10));
    	GraphsHorizontale.getChildren().add(chart);
    	GraphsHorizontale.getChildren().add(chart2);
    	GraphsHorizontale.setPrefHeight(150);
    	
    	//Affichage de la partie couleur + graphique
    	VBox affichageGeneral = new VBox();
    	affichageGeneral.setAlignment(Pos.TOP_CENTER);
    	affichageGeneral.getChildren().addAll(Couleur, GraphsHorizontale);
    	
    	Scene scene = new Scene(affichageGeneral, 525, 670);
    	
    	//On change le nom de la fenêtre en fonction du nombre de lignes de conversions de couleur
		if(nbNuance<=1) stage.setTitle(nbNuance+" Nuance de Gris");
		else stage.setTitle(nbNuance+" Nuances de Gris");
		//On empêche de pouvoir changer la taille de la fenêtre
		stage.setResizable(false);
		//On affiche la fenêtre
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * retourne un rectangle de taille fix avec un contour noir et rempli de la couleur mise en entrée
	 * @param color (String)
	 * @return Un Rectangle
	 * @author Quentin BERNARD
	 * @version 04/06/2021
	 */
	public Rectangle createRectangle(String color) {
		Rectangle rect = new Rectangle(0,0,100,75);
    	rect.setFill(Paint.valueOf(color));
	    rect.setStroke(Color.BLACK);
	    return rect;
	}
	
	/**
	 * retourne une Hbox composer de 2 rectangles, un label, un bouton et un colorPicker
	 * @param rect (Rectangle)
	 * @param color couleur que l'on souhaite convertir (String)
	 * @param vbox (Vbox)
	 * @param stage (Stage)
	 * @return Un Rectangle
	 * @author Quentin BERNARD et Hugo ROUX
	 * @version 04/06/2021
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public HBox createHBox(Rectangle rect, String color, VBox vbox, Stage stage) {
		//incrémente le nbNuance
		nbNuance++;
		
		//Initialise une nouvelle Hbox et Vbox
		HBox res = new HBox();
		VBox vbox2 = new VBox();
		
		//Initialise une nouvelle final Hbox
		final HBox hbox = new HBox();
    	hbox.setAlignment(Pos.CENTER);
    	hbox.setSpacing(50);
    	
    	VBox.setMargin(hbox, new Insets(20));
    	
    	//Initialisation du bouton "supprCouleur"
    	Button supprCouleur = new Button(" - ");
    	
    	//Ajout de l'évènement "setOnMouseReleased" 
    	//sur le bouton "supprCouleur"
    	supprCouleur.setOnMouseReleased(e1->{
    		//retire de la list des couleurs le couleurs correspondant au carré
    		couleurs.remove(rect.getFill().toString().substring(2,8));
    		//retirer la vbox de la fenêtre
    		res.getChildren().remove(vbox2);
    		//Décrémente nbNuances
    		nbNuance--;
    		//met à jour le titre de la fenêtre
    		stage.setTitle(nbNuance+" Nuances de Gris");
    		//Si nbNuances est à 9
    		if(nbNuance==9) {
    			//ne plus affcher l'option "AjouterCouleur"
    			vbox.getChildren().get(vbox.getChildren().size()-1).setVisible(true);
    		}
    		
    		//On vide GraphsHorizontale, qui stockait les camemberts
    		GraphsHorizontale.getChildren().clear();
    		//On ajoute des camemberts avec de nouvelles données
    		GraphsHorizontale.getChildren().add(updateGraphique(false));
        	GraphsHorizontale.getChildren().add(updateGraphique(true));
    	});
    	
    	//Ajout du bouton "supprCouleur"
    	hbox.getChildren().add(supprCouleur);
    	//Ajout de rectangle 
		hbox.getChildren().add(rect);
		
		//Ajout image
		URL imageURL = getClass().getResource("Arrow.png"); 
        Image image = new Image(imageURL.toExternalForm()); 
        ImageView imageView = new ImageView(image);
        
        //On change le taille de l'image
        imageView.setFitWidth(50); 
        imageView.setFitHeight(25); 
        hbox.getChildren().add(imageView); 
        
		Rectangle rectangleGris = createRectangle(conversionGris(color));
		hbox.getChildren().add(rectangleGris);
		/*
		Button espace = new Button(" - ");
		hbox.getChildren().add(espace);
		*/
		
		//Initialisation d'un colorPicker
		ColorPicker colorPicker = new ColorPicker();
		//Pose la couleur du colorPicker par défaut à la couleur du rectangle
		colorPicker.setValue(Color.color(decimalHexa(color.substring(0,2))/255, decimalHexa(color.substring(2,4))/255, decimalHexa(color.substring(4,6))/255));
		//On crée un événement sur le colorPicker
		colorPicker.setOnAction(new EventHandler() {
            public void handle(Event t) {
            	//Récupère la couleur actuelle du rectangle
            	String c = rect.getFill().toString();
            	
            	//Récupère la couleur choisie par l'utilisateur
            	String newColor = colorPicker.getValue().toString().substring(2,8);
            	//set la couleur du rectangle par celle choisie par l'utilisateur
                rect.setFill(Paint.valueOf(newColor));
                //set la couleur du rectangle par la conversion grisée de celle choisie par l'utilisateur
                rectangleGris.setFill(Paint.valueOf(conversionGris(newColor)));
                
                //on ajoute la couleur choisie par l'utilisateur dans la list des couleurs
                //à la place de l'ancienne couleur
                couleurs.add(couleurs.indexOf(c.substring(2,8)), newColor);
                couleurs.remove(c.substring(2,8));
                
                //On vide GraphsHorizontale, qui stockait les camemberts
        		GraphsHorizontale.getChildren().clear();
        		//On ajoute des camemberts avec de nouvelles données
        		GraphsHorizontale.getChildren().add(updateGraphique(false));
            	GraphsHorizontale.getChildren().add(updateGraphique(true));
            }
        });
		
		//Initialisation d'une nouvelle Hbox
		HBox hboxColorPicker = new HBox();
		
		hboxColorPicker.setSpacing(70);
		hboxColorPicker.getChildren().addAll(new Label(""), colorPicker);
		
		vbox2.getChildren().addAll(hbox, hboxColorPicker);
		
		//on ajouute une nouvelle vbox
		res.getChildren().add(vbox2);
		
		return res;
	}
	
	/**
	 * retourne la valeur numérique d'une couleur
	 * @param color que l'on souhaite manipuler (String)
	 * @return une valeur numérique
	 * @author Quentin BERNARD
	 * @version 04/06/2021
	 */
	
	public int decimalHexa(String color) {
		int res = 0;
		if(color.charAt(0)>='0' && color.charAt(0)<='9') {
			res += (color.charAt(0)-'0')*16;
		}else if(color.charAt(0)>='a' && color.charAt(0)<='f') {
			res += (color.charAt(0)-'a'+10)*16;
		}
		if(color.charAt(1)>='0' && color.charAt(1)<='9') {
			res += (color.charAt(1)-'0');
		}else if(color.charAt(1)>='a' && color.charAt(1)<='f') {
			res += (color.charAt(1)-'a'+10);
		}
		return res;
	}
	
	/**
	 * retourne la valeur de la couleur en entrée convertie en gris selon une formule
	 * @param color que l'on souhaite manipuler (String)
	 * @return Un String
	 * @author Quentin BERNARD et Hugo ROUX
	 * @version 04/06/2021
	 */
	
	public String conversionGris(String color) {
		//On récupère séparément le RGB de la couleur entrée en paramètre
		int Rouge = decimalHexa(color.substring(0, 2));
		int Vert = decimalHexa(color.substring(2, 4));
		int Bleu = decimalHexa(color.substring(4, 6));
		
		//On applique la formule numérique pour obtenir une version de gris de la couleur entrée en paramètre
		int NiveauGrisInt = (int) (0.3 * Rouge + 0.59 * Vert + 0.11 * Bleu);
		//Conversion en String
		String NiveauGrisHexa = "" + Integer.toHexString(NiveauGrisInt) + Integer.toHexString(NiveauGrisInt) + Integer.toHexString(NiveauGrisInt);
		
		return NiveauGrisHexa;
	}
	
	/**
	 * retourne pieChart (camembert) avec les données actuelles de la list "couleurs"
	 * @param gris (boolean) détermine si il s'agit du camembert avec les couleurs de bases ou avec les conversions en gris
	 * @return pieChart (camembert)
	 * @author Hugo ROUX
	 * @version 04/06/2021
	 */
	
	public PieChart updateGraphique(boolean gris){
		//Initialisation d'une ObservaleList de pieChart.data
		ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
		
		//Pour chaque couleur dans la list couleurs
		for(int i = 0; i<couleurs.size(); i++) {
			//Ajouter une nouvelle PieChart.Data
			pieChartData.add(new PieChart.Data("", 100/couleurs.size()));
		}
		
		//Initialisation d'une PieChart (camembert)
		PieChart chart = new PieChart(pieChartData);
		
		int i = 0;
		//Pour chaque couleur dans la list couleurs
	    for (PieChart.Data data : pieChartData) {
	    	//Si gris est false
	    	//changer la couleur de a portion de camembert en normal
	    	if (!gris) data.getNode().setStyle("-fx-pie-color: #" + couleurs.get(i) + ";");
	    	//changer la couleur de a portion de camembert en conversion de gris
	    	else data.getNode().setStyle("-fx-pie-color: #" + conversionGris(couleurs.get(i)) + ";");
	    	i++;
	    }
		
		chart = new PieChart(pieChartData);
		
		return chart;
	}
}
